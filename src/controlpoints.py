#!/usr/bin/env python3
# ============================================================================
# File:     controlpoints.py
# Author:   Erik Johannes Husom
# Created:  2020-04-16
# ----------------------------------------------------------------------------
# Description:
#
# ============================================================================
import random
import requests
import time


class ControlPoints():
    """Oneliner.

    Parameters
    ----------
    parameter : float
      Description.

    Attributes
    ----------
    attribute : float
       Description.

    array[float]
       Description.


    Notes
    -----
    
    References
    ----------

    Example
    -------
    >>>

    """


    def __init__(self, 
            num_of_control_points, 
            north_boundary, south_boundary,
            west_boundary, east_boundary,
            onwater_api_token):

        self.num_of_control_points = num_of_control_points
        self.control_points = []
        self.north_boundary = north_boundary
        self.south_boundary = south_boundary
        self.west_boundary = west_boundary
        self.east_boundary = east_boundary

        self.onwater_api_token = onwater_api_token
        self.timestamp = time.strftime("%Y%m%d-%H%M")

        self.generate_control_points()
        self.write_kml_file()
        self.write_gpx_file()
        

    def generate_control_points(self):

        i = 0 
        while i < self.num_of_control_points:
            lat = random.uniform(self.south_boundary, self.north_boundary)
            lon = random.uniform(self.west_boundary, self.east_boundary)
            coord = (lat, lon)

            if self._is_water(coord):
                print(f"Control point in water: {coord}.")
                continue

            if self._is_building(coord):
                continue

            if self._is_gradient(coord):
                continue

            self.control_points.append(coord)
            i += 1


    def _is_water(self, coord):
        """Check if coordinate is on water (ocean, lake, river etc)."""

        request = (
                "https://api.onwater.io/api/v1/results/"
                + str(coord[0]) + "," + str(coord[1]) 
                + "?access_token=" + self.onwater_api_token
        )

        response = requests.get(request)

        try:
            is_water =  response.json()['water']
        except KeyError:
            print("Maximum OnWater requests reached, pausing for 60 seconds.")
            time.sleep(60)
            return True

        return is_water


    def _is_building(self, coord):
        """Check if coordinate is in a building.

        TODO: Implement function.
        """

        return False


    def _is_gradient(self, coord):
        """Check if coordinate is at a place which is too steep.

        TODO: Implement function.
        """

        return False

    def write_kml_file(self):
        """Write course to a kml file."""

        filename = "orienteering-course-" + self.timestamp + ".kml"

        with open("template-header.kml", "r") as f:
            template_header = f.read()

        with open("template-footer.kml", "r") as f:
            template_footer = f.read()
        
        with open(filename, "w") as f:
            f.write(template_header)

            for i in range(self.num_of_control_points):

                # Specify name and style for start/finish/control.
                if i == 0:
                    name = "S1"
                    styleUrl = "#startfinish"
                elif i == self.num_of_control_points-1:
                    name = "F1"
                    styleUrl = "#startfinish"
                else:
                    name = str(i)
                    styleUrl = "control"
                
                # Opening tags.
                f.write("""
                    <Placemark>
                        <name>
                        """ + name + """
                        </name>
                        <styleUrl>
                            """ + styleUrl + """
                        </styleUrl>
                        <Point>
                            <gx:drawOrder>
                                1
                            </gx:drawOrder>
                            <coordinates>
                """)

                # Write coordinates. NB: Kml format is lon/lat, NOT lat/lon.
                f.write(str(self.control_points[i][1]) + ", " +
                    str(self.control_points[i][0]))

                # Close tags for control point.
                f.write("""
                            </coordinates>
                        </Point>
                    </Placemark>
                """)


            # Close all tags.
            f.write(template_footer)

        print("Orienteering course written to kml file.")


    def write_gpx_file(self):
        """Write course to gpx file."""

        filename = "orienteering-course-" + self.timestamp + ".gpx"

        
        with open(filename, "w") as f:
            f.write("""<?xml version="1.0" encoding="UTF-8"?>
<gpx version="1.1" creator="orienteering-cp-generator" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <metadata>
        <name>
            <![CDATA[Orienteering course """ + self.timestamp + """]]>
        </name>
    </metadata>""")

            for i in range(self.num_of_control_points):
                
                # Opening tags.
                f.write("<wpt lat='" 
                    + str(self.control_points[i][0]) 
                    + "' lon='" 
                    + str(self.control_points[i][1])
                    + "'><name><![CDATA["
                    + str(i)
                    + "]]></name></wpt>"
                )

            # Close all tags.
            f.write("</gpx>")

        print("Orienteering course written to gpx file.")


if __name__ == '__main__':
    
    try:
        with open("onwater-api-token.txt") as f:
            onwater_api_token = f.read().strip()
    except FileNotFoundError:
        onwater_api_token = None
        print("API token for OnWater not found, cannot check if control points are set on water.")

    n = 63.4049381
    s = 63.3856377
    w = 10.4531082
    e = 10.494001

    n = 59.6822607
    s = 59.6640721
    w = 10.7514227
    e = 10.7933044

    course = ControlPoints(8, n, s, w, e, onwater_api_token)
